#!/bin/bash
set -e

if [ $# == 1 ]
then
build=$1
else
	echo  Usage: $0 build
	exit 1
fi

fetch_artifact="/google/data/ro/projects/android/fetch_artifact"

common_args=()
common_args+=(--bid $build)
common_args+=(--target qemu_trusty_arm64-trunk_staging-userdebug)
target_zip="'qemu_trusty_arm64-target_files-${build}.zip'"

pushd pristine
rm -f adb mke2fs qemu_trusty_arm64-target_files-*.zip
$fetch_artifact "${common_args[@]}" 'adb'
$fetch_artifact "${common_args[@]}" 'mke2fs'
$fetch_artifact "${common_args[@]}" $target_zip
popd
rm -Rf android
./unpack.py
